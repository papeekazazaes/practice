﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameTwoController : MonoBehaviour
{
    // Question
    Vector3 scale, position;
    private string filePath, jsonString;
    JsonData QuizData;
    public GameObject quizPanel;
    private int numberQuiz;
    private bool nextQuestion;
    private string quizName;
    public Text questionText;

    // Answer
    public GameObject answerPrefeb;
    private GameObject answer;
    private int ranAnswer, ranNum;
    private List<int> uniqueAnswer, uniqueNumbers;

    //Sound
    private AudioSource AnswerSound, stampSound, QuestionSound;
    private string AnswerSoundPath;

    //result pop up
    public GameObject resultPopUp;
    public GameObject rightPopUp;
    public GameObject wrongPopUp;

    //score
    int score = 0;
    public Text scoreText;
    public Text resultScoreText;

    //Time
    float totalTime = 0;
    bool gameOver;
    public Text resultTimerText;
    public Text timeText;

    //BestScore
    int bestScore = 0;
    public Text bestScoreText;
    public InputField inputName;
    private string yourName;

    public Button btnSave;

    void Start()
    {
        LoadQuiz();
    }

    public void Update()
    {
        scoreText.text = score.ToString();

        //timeUpdate
        if (!gameOver)
        {
            totalTime += Time.deltaTime;
            Timer.UpdateTimer(totalTime);
            timeText.text = Timer.timeupdate;
        }
        else {
            if (string.IsNullOrEmpty(inputName.text)) {
                btnSave.interactable = false;
            }
            else { 
                btnSave.interactable = true; 
            }

        }

    }

    void LoadQuiz()
    {
        quizPanel.SetActive(true);
        resultPopUp.SetActive(false);
        rightPopUp.SetActive(false);
        wrongPopUp.SetActive(false);

        uniqueNumbers = new List<int>();
        uniqueAnswer = new List<int>();
        quizName = "Game2";
        numberQuiz = 0;
        score = 0;
        PracticeBegin();
    }

    public void PracticeBegin()
    {
        nextQuestion = true;

        TextAsset file = (TextAsset)Resources.Load("Data/Practice", typeof(TextAsset));
        jsonString = file.text;
        QuizData = JsonMapper.ToObject(jsonString);

        //add number of question add in array
        for (int k = 0; k < (QuizData[quizName].Count); k++)
        {
            uniqueNumbers.Add(k);
        }

        SetQuestion();
    }
    public void SetQuestion()
    {
        if (nextQuestion)
        {

            GameObject[] answerDestroy = GameObject.FindGameObjectsWithTag("AnswerGame1");
            if (answerDestroy != null)
            {

                for (int x = 0; x < answerDestroy.Length; x++)
                {
                    DestroyImmediate(answerDestroy[x]);
                }
            }

            Debug.Log("QuizData " + (QuizData[quizName].Count));

            //Random Question
            ranNum = uniqueNumbers[Random.Range(0, uniqueNumbers.Count)];
            Debug.Log("ranNum :" + ranNum);


            //Set QuestionText
            string questionString = QuizData[quizName][ranNum]["Question"].ToString();
            questionText.text = questionString;

            //QuestionSound
            StartCoroutine(delayOfQuestionSound());
            QuestionSound.PlayDelayed(0.5f);



            //Remove random number in uniqueNumbers
            uniqueNumbers.Remove(ranNum);

            for (int j = 0; j < (QuizData[quizName][ranNum]["Choice"].Count); j++)
            {
                uniqueAnswer.Add(j);
            }

            for (int i = 0; i < QuizData[quizName][ranNum]["Choice"].Count; i++)
            {
                answer = Instantiate(answerPrefeb);


                // Random answer unique
                ranAnswer = uniqueAnswer[Random.Range(0, uniqueAnswer.Count)];

                string answerString = QuizData[quizName][ranNum]["Choice"][ranAnswer].ToString();
                Image answerImage = answer.GetComponentInChildren<Image>();
                Sprite choiceSprite = Resources.Load("sprites/" + answerString, typeof(Sprite)) as Sprite;

                // Bind data to UI objects
                answerImage.sprite = choiceSprite;

                // Set transform of choice
                Transform choice = GameObject.Find("Choice").GetComponent<Transform>();
                answer.transform.SetParent(choice);
                scale = transform.localScale;
                position = transform.localPosition;
                position.Set(transform.position.x, transform.position.y, 0);
                scale.Set(1, 1, 1);
                answer.transform.localScale = scale;
                answer.transform.localPosition = position;

                uniqueAnswer.Remove(ranAnswer);

                // Answer onClick
                answer.GetComponent<Button>().interactable = true;
                answer.GetComponent<Button>().onClick.AddListener(() => OnChoicePressed(answerString));

            }
            numberQuiz++;
            nextQuestion = false;
            uniqueAnswer.Clear();
            Debug.Log("numberQuiz1:" + numberQuiz);

        }
    }
    public void OnChoicePressed(string userAnswer)
    {

        string resultString = QuizData[quizName][ranNum]["Answer"].ToString();
        if (userAnswer == resultString)
        {
            Debug.Log(userAnswer);
            score = score + 10;
            SetSoundAnswer(true);
            AnswerSound.PlayDelayed(0.2f);
            rightPopUp.SetActive(true);
            StartCoroutine(delayOfHidePopup());

        }
        else
        {
            //score = score - 5;
            Debug.Log(userAnswer);
            SetSoundAnswer(false);
            AnswerSound.PlayDelayed(0.2f);
            wrongPopUp.SetActive(true);
            StartCoroutine(delayOfHidePopup());

        }

    }
    IEnumerator delayOfHidePopup()
    {
        yield return new WaitForSeconds(1f);
        rightPopUp.SetActive(false);
        wrongPopUp.SetActive(false);
        StartCoroutine(nextQuestionFinish());
    }
    IEnumerator nextQuestionFinish()
    {
        yield return null;

        if (numberQuiz < 10)
        {
            nextQuestion = true;
            SetQuestion();

        }
        else
        {
            nextQuestion = false;
            gameOver = true;
            resultDisplay();
            scoreBoard.topicName = quizName;
            scoreBoard.AddScore("test", 0);
            Debug.Log("End");
        }
    }
    void SetSoundAnswer(bool isCorrect)
    {
        AnswerSoundPath = isCorrect ? "Correct" : "Incorrect";
        AnswerSound = gameObject.AddComponent<AudioSource>();
        AnswerSound.clip = Resources.Load("Sounds/" + AnswerSoundPath, typeof(AudioClip)) as AudioClip;
    }

    void resultDisplay()
    {
        resultPopUp.SetActive(true);
        resultScoreText.text = score.ToString();
        resultTimerText.text = "เวลาทั้งหมด : " + Timer.timeupdate;
        hightScore();
        bestScoreText.text = "คะแนนสูงสุด : " + bestScore.ToString();

    }

    public void saveNamePressed()
    {
        yourName = inputName.text;
        scoreBoard.AddScore(yourName, score);
        SceneManager.LoadScene(SceneName.main);
    }

    void hightScore()
    {
        int hScore = PlayerPrefs.GetInt(quizName + 0 + "HScore");
        if (score > hScore)
        {
            bestScore = score;

        }
        else
        {
            bestScore = hScore;
        }


    }

    public void replayPress()
    {
        resetTime();
        LoadQuiz();
    }

    void resetTime()
    {
        gameOver = false;
        totalTime = 0;
    }

    public void homePressed()
    {
        SceneManager.LoadScene(SceneName.main);
    }

    IEnumerator delayOfQuestionSound()
    {
        setSoundQuest(ranNum);
        GameObject.Find("btnSound").GetComponent<Button>().interactable = false;
        yield return new WaitForSeconds(QuestionSound.clip.length + 0.1f);
        GameObject.Find("btnSound").GetComponent<Button>().interactable = true;

    }

    void setSoundQuest(int ran)
    {
        string QuestionSoundName = QuizData[quizName][ran]["SoundQuestion"].ToString();
        QuestionSound = gameObject.AddComponent<AudioSource>();
        QuestionSound.clip = Resources.Load("Sounds/" + QuestionSoundName, typeof(AudioClip)) as AudioClip;
    }

    public void playSound()
    {
        setSoundQuest(ranNum);
        QuestionSound.Play();
    }
}