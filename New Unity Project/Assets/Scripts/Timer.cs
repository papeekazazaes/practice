﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Timer
{
    static int minutes;
    static int seconds;
    public static string timeupdate;
    public static void UpdateTimer(float totalSeconds)
    {
        minutes = Mathf.FloorToInt(totalSeconds / 60f);
        seconds = Mathf.RoundToInt(totalSeconds % 60f);

        string formatedSeconds = seconds.ToString();

        if (seconds == 60)
        {
            seconds = 0;
            minutes += 1;
        }

        timeupdate = minutes.ToString("00") + ":" + seconds.ToString("00");
    }
}
