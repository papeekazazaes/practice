﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class btnHomeScript : MonoBehaviour
{
    public void goToBigSmall() {
        SceneManager.LoadScene(SceneName.bigSmallGame);

    }

    public void goToColor()
    {
        SceneManager.LoadScene(SceneName.colorGame);

    }

    public void goToShape()
    {
        SceneManager.LoadScene(SceneName.shapeGame);

    }

    public void goToScoreBoard()
    {
        SceneManager.LoadScene(SceneName.scoreBoard);

    }

    public void goToMaze()
    {
        SceneManager.LoadScene(SceneName.beforeMaze);

    }
}
