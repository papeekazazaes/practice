﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class spashMazController : MonoBehaviour
{
    private AudioSource soundTopic;
    // Start is called before the first frame update
    public float timeLeft = 3.0f;

    public Text countDowntext;
    bool finish;


    void Start()
    {
        finish = false;
        SetSoundQuestion();
        StartCoroutine(delayOfFinish());
    }

    void Update()
    {
        if (finish) {
            timeLeft -= Time.deltaTime;
            countDowntext.text = (timeLeft).ToString("0");
            if (timeLeft < 0)
            {
                SceneManager.LoadScene(SceneName.mazeGame);
            }
        }
    }
    void SetSoundQuestion()
    {
        soundTopic = gameObject.AddComponent<AudioSource>();
        soundTopic.clip = Resources.Load("Sound/positive-win", typeof(AudioClip)) as AudioClip;
        soundTopic.Play();
    }
    IEnumerator delayOfFinish()
    {
        yield return new WaitForSeconds(soundTopic.clip.length);
        finish = true;
    }

}
