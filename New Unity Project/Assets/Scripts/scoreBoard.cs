﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scoreBoard : MonoBehaviour
{
    public static string topicName;
    public static int newScore;
    public static string newName;
    public static int oldScore;
    public static string oldName;

    public static void AddScore(string namescore, int scoreTotal)
    {
        newScore = scoreTotal;
        newName = namescore;

        for (int i = 0; i < 10; i++)
        {
            if (PlayerPrefs.HasKey(topicName + i + "HScore"))
            {
                if (PlayerPrefs.GetInt(topicName + i + "HScore") < newScore)
                {
                    // new score is higher than the stored score
                    oldScore = PlayerPrefs.GetInt(topicName + i + "HScore");
                    oldName = PlayerPrefs.GetString(topicName + i + "HScoreName");
                    PlayerPrefs.SetInt(topicName + i + "HScore", newScore);
                    PlayerPrefs.SetString(topicName + i + "HScoreName", newName);
                    newScore = oldScore;
                    newName = oldName;
                }
            }
            else
            {
                PlayerPrefs.SetInt(topicName + i + "HScore", newScore);
                PlayerPrefs.SetString(topicName + i + "HScoreName", newName);
                newScore = 0;
                newName = "";
            }
            Debug.Log(topicName + ":" + (PlayerPrefs.GetString(topicName + i + "HScoreName"))+","+ "" + (PlayerPrefs.GetInt(topicName + i + "HScore")));

        }
    }
    public static void DeleteData()
    {
        PlayerPrefs.DeleteAll();
    }
    public static void SetDataStart()
    {
        for (int i = 0; i < 10; i++)
        {
            PlayerPrefs.SetInt(topicName + i + "HScore", 0);
            PlayerPrefs.SetString(topicName + i + "HScoreName", "-");
        }
    }
}
