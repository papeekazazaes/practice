using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WritingHandler : MonoBehaviour
{
	public GameObject[] mazes;//the mazes list
	public static int currentMazeIndex;//the index of the current maze
	private bool clickBeganOrMovedOutOfMazeArea;//does the click began or moved out of maze area
	private int previousTracingPointIndex;//the index of the previous maze
	private ArrayList currentTracingPoints;//holds the indexes of the tracing points
	private Vector3 previousPosition, currentPosition = Vector3.zero;//thre click previous position
	public GameObject lineRendererPrefab;//the line renderer prefab
	public GameObject circlePointPrefab;//the circle point prefab
	private GameObject currentLineRender = null;//current line renderer gameobject
	public Material drawingMaterial;
	private bool mazeDone = false;
	private bool setRandomColor = true;
	private bool clickStarted;//uses with mouse input drawings,when drawing clickStarted
	public Transform hand;
	public bool showCursor;
    //Sound
	public AudioClip cheeringSound;
	public AudioClip positiveSound;
	public AudioClip wrongSound;
    // Result popup
    public GameObject dialog;
    public GameObject cheerDialog;
    //score
    int score = 0;
    int bestScore = 0;
    public Text bestScoreText;
    public Text resultTime;
    public Text totalScore;
    public InputField inputName;
    private string yourName;

    public Button btnSave;

    // Time
    float totalTime = 0;
    bool gameOver;
    public Text timeText;
    // Random maze
    public static int numberOfMaze;
    private int ranNumIndex;
    private List<int> uniqueNumIndex;

    IEnumerator Start ()
	{
        Cursor.visible = showCursor;//show curosr or hide
		currentTracingPoints = new ArrayList ();//initiate the current tracing points
        //List ints only
        uniqueNumIndex = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7};
        scoreBoard.topicName = "Game4";
        LoadMaze ();
				
		yield return 0;
	}


		//Executes Every Single Frame
    void Update ()
    {
		if (mazeDone) {//if the maze is done then skip the next
				return;
		}

		if (Input.GetKeyDown (KeyCode.Escape)) {//on escape pressed
				BackToMenu ();//back to menu 
		}

		RaycastHit2D hit2d = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);//raycast hid c

		if (hit2d.collider != null) {
			if (Input.GetMouseButtonDown (0)) {
				TouchMazeHandle (hit2d.collider.gameObject, true, Camera.main.ScreenToWorldPoint (Input.mousePosition));//touch for maze move(drawing);
                clickStarted = true;
			} else if (clickStarted) {
                TouchMazeHandle(hit2d.collider.gameObject, false, Camera.main.ScreenToWorldPoint (Input.mousePosition));//touch for maze move(drawing);
			}  
		}
		if (Input.GetMouseButtonUp (0)) {

			if (clickStarted) {
				EndTouchMazeHandle ();
				clickStarted = false;
				clickBeganOrMovedOutOfMazeArea = false;
			}
		}

		if (hand != null) {
				//drag the hand on screen
			Vector3 clickPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			clickPosition.z = -6;
			hand.position = clickPosition;
		}

        //Timer update
        if (!gameOver)
        {
            totalTime += Time.deltaTime;
            Timer.UpdateTimer(totalTime);
            timeText.text = Timer.timeupdate;
        }
        else
        {
            // End game save button is disable
            if (string.IsNullOrEmpty(inputName.text))
            {
                btnSave.interactable = false;
            }
            else
            {
                btnSave.interactable = true;
            }

        }
    }

		//maze touch hanlder
	private void TouchMazeHandle (GameObject ob, bool isTouchBegain, Vector3 touchPos)
	{
        string obTag = ob.tag;// name of button that ray hit it
        bool flag1 = (obTag == "Maze" || obTag == "TracingPoint" || obTag == "Background") && currentLineRender != null;
        bool flag2 = (obTag == "TracingPoint");

        if (flag1 && !isTouchBegain) {//Touch Moved
            if (obTag == "TracingPoint") {
                TracingPoint tracingPoint = ob.GetComponent<TracingPoint> ();//get the current tracing point
                int currentindex = tracingPoint.index;//get the tracing point index
                if (tracingPoint.single_touch) {//skip if the touch is single
                    return;
                }

                if (currentindex != previousTracingPointIndex) {
                    currentTracingPoints.Add (currentindex);
                    //add the current tracing point to the list
                    previousTracingPointIndex = currentindex;//set the previous tracing point
                }
            } else if (obTag == "Background") {
                clickBeganOrMovedOutOfMazeArea = true;
                EndTouchMazeHandle ();
                clickStarted = false;
                return;
            }

            currentPosition = touchPos;
            currentPosition.z = -5.0f;
            float distance = Mathf.Abs (Vector3.Distance (currentPosition, new Vector3 (previousPosition.x, previousPosition.y, currentPosition.z)));//the distance between the current touch and the previous touch
            if (distance <= 0.1f) {//0.1 is distance offset
                return;
            }

            previousPosition = currentPosition;//set the previous position

            InstaitaeCirclePoint (currentPosition, currentLineRender.transform);//create circle point

            //add the current point to the current line
            LineRenderer ln = currentLineRender.GetComponent<LineRenderer> ();
            LineRendererAttributes line_attributes = currentLineRender.GetComponent<LineRendererAttributes> ();
            int numberOfPoints = line_attributes.NumberOfPoints;
            numberOfPoints++;
            line_attributes.Points.Add (currentPosition);
            line_attributes.NumberOfPoints = numberOfPoints;
            ln.positionCount = numberOfPoints;
            ln.SetPosition (numberOfPoints - 1, currentPosition);
        } else if (flag2 && isTouchBegain) {//Touch Began
            TracingPoint tracingPoint = ob.GetComponent<TracingPoint> ();//get the tracing point
            int currentindex = tracingPoint.index;//get the tracing point index
            if (currentindex != previousTracingPointIndex) {
                currentTracingPoints.Add (currentindex);//add the current tracing point to the list
                previousTracingPointIndex = currentindex;//set the previous tracing point

                if (currentLineRender == null) {
                    currentLineRender = (GameObject)Instantiate (lineRendererPrefab);//instaiate new line
                    if (setRandomColor) {
                        currentLineRender.GetComponent<LineRendererAttributes> ().SetRandomColor ();//set a random color for the line
                        setRandomColor = false;
                    }
                }
                Vector3 currentPosition = touchPos;//ge the current touch position
                currentPosition.z = -5.0f;
                previousPosition = currentPosition;//set the previous position

                if (tracingPoint.single_touch) {
                    InstaitaeCirclePoint (currentPosition, currentLineRender.transform);//create circle point
                } else {
                    InstaitaeCirclePoint (currentPosition, currentLineRender.transform);//create circle point

                    //add the current point to the current line
                    LineRenderer ln = currentLineRender.GetComponent<LineRenderer> ();
                    LineRendererAttributes line_attributes = currentLineRender.GetComponent<LineRendererAttributes> ();
                    int numberOfPoints = line_attributes.NumberOfPoints;
                    numberOfPoints++;
                    if (line_attributes.Points == null) {
                        line_attributes.Points = new List<Vector3> ();
                    }

                    line_attributes.Points.Add (currentPosition);
                    line_attributes.NumberOfPoints = numberOfPoints;
                    ln.positionCount = numberOfPoints;
                    ln.SetPosition (numberOfPoints - 1, currentPosition);
                }
            }
        } 
	}

	//On tocuh released
	private void EndTouchMazeHandle ()
	{
        if (currentLineRender == null || currentTracingPoints.Count == 0) {
            return;//skip the next
        }

        TracingPart [] tracingParts = mazes [currentMazeIndex].GetComponents<TracingPart> ();//get the tracing parts of the current maze
        bool equivfound = false;//whether a matching or equivalent tracing part found
        if (!clickBeganOrMovedOutOfMazeArea) {

            foreach (TracingPart part in tracingParts) {//check tracing parts
                if (currentTracingPoints.Count == part.order.Length && !part.succeded) {
                    if (PreviousMazesPartsSucceeded (part, tracingParts)) {//check whether the previous tracing parts are succeeded
                        equivfound = true;//assume true
                        for (int i =0; i < currentTracingPoints.Count; i++) {
                            int index = (int)currentTracingPoints [i];
                            if (index != part.order [i]) {
                                equivfound = false;
                                break;
                            }
                        }
                    }
                }
                if (equivfound) {//if equivalent found
                    part.succeded = true;//then the tracing part is succeed (written as wanted)
                    break;
                }
            }
        }

        if (equivfound) {//if equivalent found

            if (currentTracingPoints.Count != 1) {
                StartCoroutine ("SmoothCurrentLine");//make the current line smoother
            } else {
                currentLineRender = null;
            }
            PlayPositiveSound ();//play positive sound effect
        } else {
            PlayWrongSound ();//play negative or wrong answer sound effect
            Destroy (currentLineRender);//destroy the current line
            currentLineRender = null;//release the current line
        }

        previousPosition = Vector2.zero;//reset previous position
        currentTracingPoints.Clear ();//clear record of indexed
        previousTracingPointIndex = 0;//reset previous selected Index(index as point id)
        CheckMazeDone();//check if the entier maze is written successfully or done
        if (mazeDone) {//if the current maze done or wirrten successfully
            if (cheeringSound != null)
                AudioSource.PlayClipAtPoint (cheeringSound, Vector3.zero, 0.8f);//play the cheering sound effect
            hand.GetComponent<SpriteRenderer> ().enabled = false;//hide the hand
        }
	}

	//Check maze done or not
	private void CheckMazeDone ()
	{
        bool success = true;//maze success or done flag
        TracingPart [] tracingParts = mazes [currentMazeIndex].GetComponents<TracingPart> ();//get the tracing parts of the current maze
        foreach (TracingPart part in tracingParts) {
            if (!part.succeded) {
                success = false;
                break;
            }
        }

        if (success) {
            mazeDone = true;//maze done flag

            GameObject [] linesRenderes = GameObject.FindGameObjectsWithTag ("LineRenderer");
            foreach (GameObject line in linesRenderes) {
                line.GetComponent<LineRenderer> ().enabled = false;
            }

            GameObject [] circlePoint = GameObject.FindGameObjectsWithTag ("CirclePoint");
            foreach (GameObject cp in circlePoint) {
                cp.GetComponent<MeshRenderer> ().enabled = false;
            }

            mazes [currentMazeIndex].SetActive (false);
            // Next maze
            StartCoroutine(delayOfNextMaze());
        }
    }
	//Refresh the lines and reset the tracing parts
	public void RefreshProcess ()
	{
        RefreshLines ();
		TracingPart [] tracingParts = mazes [currentMazeIndex].GetComponents<TracingPart> ();
		foreach (TracingPart part in tracingParts) {
			part.succeded = false;
		}
		if (hand != null)
		hand.GetComponent<SpriteRenderer> ().enabled = true;
        mazeDone = false;
	}

	//Refreesh the lines
	private void RefreshLines ()
	{
		StopCoroutine ("SmoothCurrentLine");
		GameObject [] gameobjs = HierrachyManager.FindActiveGameObjectsWithTag ("LineRenderer");
		if (gameobjs == null) {
			return;
		}
		foreach (GameObject gob in gameobjs) {
			Destroy (gob);	
		}
	}

	//Make the current lime more smoother
	private IEnumerator SmoothCurrentLine ()
	{
        LineRendererAttributes line_attributes = currentLineRender.GetComponent<LineRendererAttributes> ();
		LineRenderer ln = currentLineRender.GetComponent<LineRenderer> ();
		Vector3[] vectors = SmoothCurve.MakeSmoothCurve (line_attributes.Points.ToArray (), 10);
		
		int childscount = currentLineRender.transform.childCount;
		for (int i = 0; i < childscount; i++) {
            Destroy (currentLineRender.transform.GetChild (i).gameObject);
		}
		
		line_attributes.Points.Clear ();
		for (int i = 0; i <vectors.Length; i++) {
            if (i == 0 || i == vectors.Length - 1)
			InstaitaeCirclePoint (vectors [i], currentLineRender.transform);
            line_attributes.NumberOfPoints = i + 1;
            line_attributes.Points.Add (vectors [i]);
            ln.positionCount = (i + 1);
            ln.SetPosition (i, vectors [i]);
        }
		currentLineRender = null;
		yield return new WaitForSeconds (0);
	}

	//Check If User Passed The Previous Parts Before The Give maze Part
	public static bool PreviousMazesPartsSucceeded (TracingPart currentpart, TracingPart[] lparts)
	{
		int p = currentpart.priority;

		if (p == 1) {
			return true;
		}

		bool prevsucceded = true;
		foreach (TracingPart part in lparts) {
			if (part.priority < p) {
				if (!part.succeded && part.order.Length != 1) {//make single point TracingParts have no priority
					prevsucceded = false;
					break;
				}
			}
		}

		return prevsucceded;
	}

	//Play a positive or correct sound effect
	private void PlayPositiveSound ()
	{
			if (positiveSound != null)
					AudioSource.PlayClipAtPoint (positiveSound, Vector3.zero, 0.8f);//play the cheering sound effect
	}

	//Play wrong or opps sound effect
	private void PlayWrongSound ()
	{
        if (wrongSound != null)
            AudioSource.PlayClipAtPoint (wrongSound, Vector3.zero, 0.8f);//play the cheering sound effect
	}
    IEnumerator delayOfNextMaze()
    {
        cheerDialog.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        cheerDialog.SetActive(false);
        LoadNextMaze();
    }
    //Load the next maze
    public void LoadNextMaze ()
		{
        if (currentMazeIndex >= 0 && numberOfMaze < 4)
        {
            mazes[currentMazeIndex].SetActive(true);
            uniqueNumIndex.Remove(ranNumIndex);
            numberOfMaze++;
            LoadMaze();
        }
        else
        {
            showDialog();
        }
    }

	//Load the current maze
	private void LoadMaze ()
	{
        ranNumIndex = uniqueNumIndex[Random.Range(0, uniqueNumIndex.Count)];
        Debug.Log("ranNumIndex : " + ranNumIndex);
        currentMazeIndex = ranNumIndex;

        if (mazes == null) {
            return;
        }

        if (!(currentMazeIndex >= 0 && currentMazeIndex < mazes.Length)) {
            return;
        }

        if (mazes [currentMazeIndex] == null) {
            return;
        }
        mazeDone = false;
        RefreshProcess ();
        HideMazes();

        mazes [currentMazeIndex].SetActive (true);

        setRandomColor = true;
	}

	//Hide the mazes
	private void HideMazes ()
	{
        if (mazes == null) {
            return;
        }

        foreach (GameObject maze in mazes) {
            if (maze != null)
                maze.SetActive (false);
        }
	}
	
		//Create Cicle at given Point
	private void InstaitaeCirclePoint (Vector3 position, Transform parent)
	{
		GameObject currentcicrle = (GameObject)Instantiate (circlePointPrefab);//instaiate object
		currentcicrle.transform.parent = parent;
		currentcicrle.GetComponent<Renderer>().material = currentLineRender.GetComponent<LineRendererAttributes> ().material;
		currentcicrle.transform.position = position;
	}

    void showDialog()
    {
        gameOver = true;
        dialog.SetActive(true);
        RefreshProcess();
        resultTime.text = "Total Time : " + Timer.timeupdate;
        calScore();
        hightScore();
    }
    void hightScore()
    {
        int bScore = PlayerPrefs.GetInt(scoreBoard.topicName + 0 + "HScore");
        if (score > bScore)
        {
            bestScore = score;
        }
        else
        {
            bestScore = bScore;
        }
        Debug.Log("HScore :" +bScore);
        bestScoreText.text = "Best Score : " + bestScore.ToString();
    }
    void calScore()
    {
        if (totalTime <= 75)
        {
            score = 100;
        }else if (totalTime <= 150){
            score = 75;
        }else if (totalTime <= 225)
        {
            score = 50;
        }
        else
        {
            score = 25;
        }
        totalScore.text = score.ToString();
    }
    public void savePressed()
    {
        yourName = inputName.text;
        scoreBoard.AddScore(yourName, score);
        SceneManager.LoadScene(SceneName.main);
    }

    //Back To Menu
    public void BackToMenu()
    {
        SceneManager.LoadScene(SceneName.main);
    }
    public void replayGame()
    {
        uniqueNumIndex = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7};
        score = 0;
        numberOfMaze = 0;
        gameOver = false;
        totalTime = 0;
        dialog.SetActive(false);
        LoadMaze();
    }
}
