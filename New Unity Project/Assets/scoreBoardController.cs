﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class scoreBoardController : MonoBehaviour
{
    Vector3 scale, position;
    public GameObject rankPrefeb;
    private GameObject rank;
    string topicNamebutton;
    string nameString;

    void Start()
    {
        topicNamebutton = "Game1";
        loadScoreBoard();
    }
    public void loadScoreBoard()
    {
            GameObject[] rankDestroy = GameObject.FindGameObjectsWithTag("Rank");
            if (rankDestroy != null)
            {
                for (int x = 0; x < rankDestroy.Length; x++)
                {
                    DestroyImmediate(rankDestroy[x]);
                }
            }
            for (int i = 0; i < 10; i++)
            {
                rank = Instantiate(rankPrefeb);
                nameString = PlayerPrefs.GetString(topicNamebutton + i + "HScoreName");
                string scoreSting = PlayerPrefs.GetInt(topicNamebutton + i + "HScore").ToString();
                Text[] rankText;
                rankText = rank.GetComponentsInChildren<Text>();
                rankText[0].text = "" + (i + 1);
                rankText[1].text = "" + nameString;
                rankText[2].text = "" + scoreSting;

                if (i < 5)
                {
                    Transform container = GameObject.Find("scoreBoardPanel1-5").GetComponent<Transform>();
                    rank.transform.SetParent(container);

                }
                else
                {
                    Transform container = GameObject.Find("scoreBoardPanel6-10").GetComponent<Transform>();
                    rank.transform.SetParent(container);
                }

                scale = transform.localScale;
                position = transform.localPosition;
                position.Set(transform.position.x, transform.position.y, 0);
                scale.Set(1, 1, 1);
                rank.transform.localScale = scale;
                rank.transform.localPosition = position;
            }
    }

    public void gameOnePressed()
    {
        topicNamebutton = "Game1";
        //scoreBoard.topicName = topicNamebutton;
        //scoreBoard.SetDataStart();
        loadScoreBoard();
    }
    public void gameTwoPressed()
    {
        topicNamebutton = "Game2";
        loadScoreBoard();
    }
    public void gameThreePressed()
    {
        topicNamebutton = "Game3";
        loadScoreBoard();
    }
    public void gameFourPressed()
    {
        topicNamebutton = "Game4";
        loadScoreBoard();
    }

    public void homePressed()
    {
        SceneManager.LoadScene(SceneName.main);
    }
}
